import store from '../store';
export default {
    successNotificacion(mjs, time = 5000){
        store.commit("SET_NOTIFICATION", {
            display: true,
            text: mjs,
            alertClass: 'success',
            time: time
        });
    },
    errorNotificacion(error, time = 5000){
        store.commit("SET_NOTIFICATION", {
            display: true,
            text: error,
            alertClass: 'error',
            time: time
        });
    }
}