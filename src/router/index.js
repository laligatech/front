import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Inicio.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path: '/clubes',
    name: 'Clubes',
    component: () => import(/* webpackChunkName: "clubes" */ '../components/Clubes/Clubes')
  },
  {
    path: '/jugadores',
    name: 'Jugadores',
    component: () => import(/* webpackChunkName: "jugadores" */ '../components/Usuarios/Usuarios')
  },
  {
    path: '/perfiles',
    name: 'Perfiles',
    component: () => import(/* webpackChunkName: "perfiles" */ '../components/Perfiles/Perfiles')
  },
  {
      path: '/altasbajas',
      name: 'AltasBajas',
      component: () => import(/* webpackChunkName: "altasbajas" */ '../components/Perfiles/Perfiles')
    },

]

const router = new VueRouter({
  mode: process.env.NODE_ENV === 'development' ? 'history' : 'hash',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

export default router
