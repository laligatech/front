import axios from 'axios';
import notificacionService from "../../services/notificacionService";
export default {
    state: {
        clubes: []
    },
    mutations: {
        SET_CLUBES: (state, payload) => {
            state.clubes = payload;
        },
    },
    actions: {
        GET_CLUBES: ({commit}) => {
            commit("SET_LOADER", true);
            axios.get(`clubes/list`)
                .then((res)=>{
                    // console.log('clubes desde el modulo: ',res.data);
                    commit("SET_CLUBES", res.data);
                    commit("SET_LOADER", false);
                })
                .catch((error)=>{
                    notificacionService.errorNotificacion(error)
                })

        }
    },
    getters: {
        CLUBES: state => {
            return state.clubes;
        },
    }
}
