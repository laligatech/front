import axios from "axios";
import notificacionService from '../../services/notificacionService'

export default {
    state: {
        username: '',
        notification: {
            display: false,
            text: "Notification",
            timeout: 3000,
            class: "success"
        },
        loader: false,
        posiciones: []
    },
    mutations: {
        SET_NOTIFICATION: (state, {display, text, alertClass, time}) => {
            state.notification.display = display;
            state.notification.text = text;
            state.notification.class = alertClass;
            state.notification.timeout = time;
        },
        SET_LOADER: (state, loader) => {
            state.loader = loader;
        },
        SET_POSICIONES: (state, payload) => {
            state.posiciones = payload;
        },
    },
    actions: {
        GET_POSICIONES: ({commit}) => {
            commit("SET_LOADER", true);
            axios.get(`posiciones/list`)
                .then((res)=>{
                    commit("SET_POSICIONES", res.data);
                    commit("SET_LOADER", false);
                })
                .catch((error)=>{
                    notificacionService.errorNotificacion(error)
                })

        }
    },
    getters: {
        NOTIFICATION: state => {
            return state.notification;
        },
        LOADER: state => {
            return state.loader;
        },
        POSICIONES: state => {
            return state.posiciones;
        },
    }
}
