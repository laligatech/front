import axios from 'axios';
import notificacionService from "../../services/notificacionService";
export default {
    state: {
        perfiles: []
    },
    mutations: {
        SET_PERFILES: (state, payload) => {
            state.perfiles = payload;
        },
    },
    actions: {
        GET_PERFILES: ({commit}) => {
            commit("SET_LOADER", true);
            axios.get(`perfiles/list`)
                .then((res)=>{
                    commit("SET_PERFILES", res.data);
                    commit("SET_LOADER", false);
                })
                .catch((error)=>{
                    notificacionService.errorNotificacion(error)
                })
        }
    },
    getters: {
        PERFILES: state => {
            return state.perfiles;
        },
    }
}
