import Vue from 'vue'
import Vuex from 'vuex'
import clubes from "./modules/clubes";
import uiM from "./modules/ui";
import perfiles from "./modules/perfiles";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  mutations: {
  },
  actions: {
  },
  modules: {
    cb: clubes,
    ui: uiM,
    pf: perfiles
  }
})
